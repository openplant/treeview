﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace OpenPlant
{
    /// <summary>
    /// Interaction logic for UserLogControl.xaml
    /// </summary>
    public partial class UserLogControl : UserControl
    {
        public LogControlDataViewModel logControlDataViewModel;


        public UserLogControl()
        {
            Global.Products.Add("LogServer", new ProductDetails()
            {
                ProductName = "LogServer",
                ProgramDataDirectory = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\LogServer",
                LogFilePath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\Logs\\LS.log",
                ProductShortName = "LS"

            });
            Logger.Log("Initiating Application", Global.Products["LogServer"].LogFilePath);
            InitializeComponent();    

            Conference western = new Conference();
            western.Name = "Western";
            western.Teams = new List<Team>();

            Team team1 = new Team { Name="Del Porto" };
            team1.Players = new List<Player>();
            team1.Players.Add(new Player { Name = "Janga", Comment = "Jinadfads adfas asdfasf" });
            team1.Players.Add(new Player { Name = "Kambi", Comment = "Jinadfads adfas asdfasf" });
            team1.Players.Add(new Player { Name = "Chunga", Comment = "Jinadfads adfas asdfasf" });
            team1.Players.Add(new Player { Name = "Ranga Mamu", Comment = "Jinadfads adfas asdfasf" });
            team1.Players.Add(new Player { Name = "Chumma Apa", Comment = "Jinadfads adfas asdfasf" });
            team1.Players.Add(new Player { Name = "Boro Khalu", Comment = "Jinadfads adfas asdfasf" });


            Team team2 = new Team { Name = "Houston Dynamo" };
            team2.Players = new List<Player>();
            team2.Players.Add(new Player { Name = "Janga", Comment="Jinadfads adfas asdfasf" });
            team2.Players.Add(new Player { Name = "Kambi", Comment = "Jinadfads adfas asdfasf" });
            team2.Players.Add(new Player { Name = "Chunga", Comment = "Jinadfads adfas asdfasf" });
            team2.Players.Add(new Player { Name = "Ranga Mamu", Comment = "Jinadfads adfas asdfasf" });
            team2.Players.Add(new Player { Name = "Chumma Apa", Comment = "Jinadfads adfas asdfasf" });
            team2.Players.Add(new Player { Name = "Boro Khalu", Comment = "Jinadfads adfas asdfasf" });

            Team team3 = new Team { Name = "Los Angeles Galaxy" };
            team3.Players = new List<Player>();
            team3.Players.Add(new Player { Name = "Janga", Comment = "Jinadfads adfas asdfasf" });
            team3.Players.Add(new Player { Name = "Kambi", Comment = "Jinadfads adfas asdfasf" });
            team3.Players.Add(new Player { Name = "Chunga", Comment = "Jinadfads adfas asdfasf" });
            team3.Players.Add(new Player { Name = "Ranga Mamu", Comment = "Jinadfads adfas asdfasf" });
            team3.Players.Add(new Player { Name = "Chumma Apa", Comment = "Jinadfads adfas asdfasf" });
            team3.Players.Add(new Player { Name = "Boro Khalu", Comment = "Jinadfads adfas asdfasf" });

            Team team4 = new Team { Name = "Real Salt Lake" };
            team4.Players = new List<Player>();
            team4.Players.Add(new Player { Name = "Janga", Comment = "Jinadfads adfas asdfasf" });
            team4.Players.Add(new Player { Name = "Kambi", Comment = "Jinadfads adfas asdfasf" });
            team4.Players.Add(new Player { Name = "Chunga", Comment = "Jinadfads adfas asdfasf" });
            team4.Players.Add(new Player { Name = "Ranga Mamu", Comment = "Jinadfads adfas asdfasf" });
            team4.Players.Add(new Player { Name = "Chumma Apa", Comment = "Jinadfads adfas asdfasf" });
            team4.Players.Add(new Player { Name = "Boro Khalu", Comment = "Jinadfads adfas asdfasf" });

            Team team5 = new Team { Name = "San Jose Earthquakes" };
            team5.Players = new List<Player>();
            team5.Players.Add(new Player { Name = "Janga", Comment = "Jinadfads adfas asdfasf" });
            team5.Players.Add(new Player { Name = "Kambi", Comment = "Jinadfads adfas asdfasf" });
            team5.Players.Add(new Player { Name = "Chunga", Comment = "Jinadfads adfas asdfasf" });
            team5.Players.Add(new Player { Name = "Ranga Mamu", Comment = "Jinadfads adfas asdfasf" });
            team5.Players.Add(new Player { Name = "Chumma Apa", Comment = "Jinadfads adfas asdfasf" });
            team5.Players.Add(new Player { Name = "Boro Khalu", Comment = "Jinadfads adfas asdfasf" });


            western.Teams.Add(team1);
            western.Teams.Add(team2);
            western.Teams.Add(team3);
            western.Teams.Add(team4);
            western.Teams.Add(team5);

            Conference eastern = new Conference();
            eastern.Name = "Eastern";
            eastern.Teams = new List<Team>();
            eastern.Teams.Add(team1);
            eastern.Teams.Add(team2);
            eastern.Teams.Add(team3);
            eastern.Teams.Add(team4);
            eastern.Teams.Add(team5);



            Collection<Conference> league = new Collection<Conference>();
            league = new Collection<Conference>() { western, eastern };


            //league = new 

            logControlDataViewModel = new LogControlDataViewModel()
            {
                LogToShow = "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                ButtonText = "Grab the log of the server",
                WesternConference = western,
                EasternConference = eastern,
                League = league
            };


            //this.DataContext = logControlDataViewModel;

            TestViewModel testViewModel = new TestViewModel()
            {
                DataToShow = "adsfasdfasdasdfasdfasf"
            };
            this.DataContext = testViewModel;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void OnTreeViewSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Type TT = treeView1.SelectedItem.GetType();
            if (TT == typeof(Player))
            {
                Player selectedItem = (Player)treeView1.SelectedItem;
                textBlock.Text = (string)selectedItem.Comment;
            }
            else if(TT == typeof(Conference))
            {
                Conference selectedItem = (Conference)treeView1.SelectedItem;
                textBlock.Text = (string)selectedItem.Name;
            }
            else if(TT == typeof(Team))
            {
                Team selectedItem = (Team)treeView1.SelectedItem;
                textBlock.Text = (string)selectedItem.Name;
            }
            
        }
    }




    public class ExtendedTreeView : TreeView
    {
        public ExtendedTreeView()
            : base()
        {
            this.SelectedItemChanged += new RoutedPropertyChangedEventHandler<object>(___ICH);
        }

        void ___ICH(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (SelectedItem != null)
            {
                SetValue(SelectedItem_Property, SelectedItem);
            }
        }

        public object SelectedItem_
        {
            get { return (object)GetValue(SelectedItem_Property); }
            set { SetValue(SelectedItem_Property, value); }
        }
        public static readonly DependencyProperty SelectedItem_Property = DependencyProperty.Register("SelectedItem_", typeof(object), typeof(ExtendedTreeView), new UIPropertyMetadata(null));
    }

    public class Player
    {
        public string Name { get; set; }
        public string Comment { get; set; }
        
    }

    public class Team
    {
        public string Name { get; set; }
        public List<Player> Players { get; set; }
    }

    public class Conference
    {
        public string Name { get; set; }
        public List<Team> Teams { get; set; }
    }

    //public class League
    //{
    //    public Collection<Conference> Conferences { get; set;}
    //}


    public partial class LogControlDataViewModel : INotifyPropertyChanged
    {
        #region BOILER PLATE - PROPERTY CHANGED
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        protected bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }
        #endregion

        string _LogToShow = ""; public String LogToShow { get { return _LogToShow; } set { SetField(ref _LogToShow, value, "LogToShow"); } }
        string _ButtonText = ""; public String ButtonText { get { return _ButtonText; } set { SetField(ref _ButtonText, value, "ButtonText"); } }


        //League _League ; public League League { get { return _League; } set { SetField(ref _League, value, "League"); } }        

        Conference _EasternConference; public Conference EasternConference { get { return _EasternConference; } set { SetField(ref _EasternConference, value, "EasternConference"); } }
        Conference _WesternConference; public Conference WesternConference { get { return _WesternConference; } set { SetField(ref _WesternConference, value, "WesternConference"); } }
        public Collection<Conference> _League; public Collection<Conference> League { get { return _League; } set { SetField(ref _League, value, "League"); } }
    }

   
}
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Data;
using System.IO;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Windows;

namespace OpenPlant
{
    public class TestContract : iTestContract
    {
        
        
        public Post GetPostData()
        {
            Post post = new Post();
            string someStringFromColumnZero = "";
            //return post;


            //Get data from db
            var dbCon = DBConnection.Instance();
            dbCon.DatabaseName = "eculight";
            if (dbCon.IsConnect())
            {
                //suppose col0 and col1 are defined as VARCHAR in the DB
                string query = "SELECT `post_content` FROM `wp_posts` WHERE `ID` = 48; ";
                var queryExecution = new MySqlCommand(query, dbCon.Connection);
                var result = queryExecution.ExecuteReader();
                while (result.Read())
                {
                    someStringFromColumnZero = result.GetString(0);              
                }
            }
            post.post_name = someStringFromColumnZero;


            
            return post;
        }

        
        

        public bool SendPostData(List<Post> listPost)
        {
            
            TestViewModel testViewModel1 = new TestViewModel()
            {
                ListPost = new List<Post>()
            };

            testViewModel1.ListPost = listPost;
            ((Window1)System.Windows.Application.Current.MainWindow).DataContext = testViewModel1;

            return true;
        }

        public DateTime GetServerTimeUTC()
        {
            return DateTime.UtcNow;
        }

        public int ServerBackupTime()
        {
            return 10;
        }

        public string SendMessage(string msg)
        {
            TestViewModel testViewModel = new TestViewModel();
            testViewModel.DataToShow = msg;
            ((Window1)System.Windows.Application.Current.MainWindow).DataContext = testViewModel;

            return msg;
        }
    }



    public class Post
    {
        public string post_name { get; set; }
        public string post_title { get; set; }
    }


    public class DBConnection
    {
        private DBConnection()
        {
        }

        private string databaseName = string.Empty;
        public string DatabaseName
        {
            get { return databaseName; }
            set { databaseName = value; }
        }

        public string Password { get; set; }
        private MySqlConnection connection = null;
        public MySqlConnection Connection
        {
            get { return connection; }
        }

        private static DBConnection _instance = null;
        public static DBConnection Instance()
        {
            if (_instance == null)
                _instance = new DBConnection();
            return _instance;
        }

        public bool IsConnect()
        {
            bool result = true;
            if (Connection == null)
            {
                if (String.IsNullOrEmpty(databaseName))
                    result = false;
                string connstring = string.Format("Server=localhost; database={0}; UID=root; password=", databaseName);
                connection = new MySqlConnection(connstring);
                connection.Open();
                result = true;
            }

            return result;
        }

        public void Close()
        {
            connection.Close();
        }
    }
}

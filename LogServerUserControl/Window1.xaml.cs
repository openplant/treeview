﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.ObjectModel;
namespace OpenPlant
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public TestViewModel testViewModel;
        public object ItemSelected;
        public Window1()
        {
            InitializeComponent();
            testViewModel = new TestViewModel();
            this.DataContext = testViewModel;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            testViewModel.DataToShow = "YOU CLICKED ME!!";
            testViewModel.DataInput = "FORM FILLED!!";
            testViewModel.ButtonClicked = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            WCFHost<TestContract, iTestContract> WcfHost = new WCFHost<TestContract, iTestContract>(Global.Products["LogServer"],33173);
            WcfHost.Start();


        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {

            testViewModel.PlantNames.Remove(testViewModel.SelectedItem);
        }

        private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }
    }

    public partial class TestViewModel:INotifyPropertyChanged
    {        
        #region BOILER PLATE - PROPERTY CHANGED
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        protected bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }
        #endregion

        string _DataToShow = "";public String DataToShow { get { return _DataToShow; } set { SetField(ref _DataToShow, value, "DataToShow"); } }
        string _DataInput = "";public String DataInput { get { return _DataInput; } set { SetField(ref _DataInput, value, "DataInput"); } }

        List<Post> _ListPost; public List<Post> ListPost { get { return _ListPost; } set { SetField(ref _ListPost, value, "ListPost"); } }

        //public List<Post> ListPost; 
        bool _ButtonClicked = false; public bool ButtonClicked { get { return _ButtonClicked; } set { SetField(ref _ButtonClicked, value, "ButtonClicked"); } }
        public ObservableCollection<String> PlantNames { get; set; }
        string _SelectedItem; public string SelectedItem { get { return _SelectedItem; } set { SetField(ref _SelectedItem, value, "SelectedItem"); } }
    }
}

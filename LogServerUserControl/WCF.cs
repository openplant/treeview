using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Data;
using System.IO;
using System.Timers;
using System.ServiceModel.Channels;
using System.ServiceModel.Activation;


namespace OpenPlant
{
    public class WCFHost<clsType,iContract>
    {
        Timer WCFConnectionTimer = new Timer();
        ServiceHost ServiceHost = null;
        //Constructors =================================================================
        public WCFHost(ProductDetails ProductDetails, int ListeningPort, int RefreshIntervalInMS = 15000)
        {
            this.ProductDetails = ProductDetails;
            this.RefreshIntervalInMS = RefreshIntervalInMS;
            WCFConnectionTimer.Elapsed += new ElapsedEventHandler(WCFConnectionTimer_Elapsed); ;
            this.ListeningPort = ListeningPort;
        }
        //Properties ===================================================================
        public ProductDetails ProductDetails { get; set; }
        public Host HostProtocol { get; set; }
        public int ListeningPort { get; set; }
        public int RefreshIntervalInMS = 15000;

        //Methods ======================================================================
        public enum Host { TCP = 1, HTTP = 2};
        public void Start()
        {
            this.HostProtocol = HostProtocol;
            this.ListeningPort = ListeningPort;
            if (!_IsEnabled)
            {
                this.HostWCFTCP(); //Initialize for the first time
                WCFConnectionTimer_Elapsed(null, null);
                WCFConnectionTimer.Interval = Convert.ToDouble(this.RefreshIntervalInMS);
                WCFConnectionTimer.Enabled = true;
                Logger.Log("WCF Connection Timer for '" + typeof(clsType) + "' Started. Timer Refreshes every " + WCFConnectionTimer.Interval + "ms", this.ProductDetails.LogFilePath);
            }
        }

        public void Stop()
        {
            this.ServiceHost.Close();
            _IsEnabled = false;
        }

        bool _IsEnabled = false; public bool IsEnabled { get { return _IsEnabled; } }
        bool _WCFinexecution = false;
        readonly object LockWCFConnectionTimer = new Object();
        private void WCFConnectionTimer_Elapsed(object sender, EventArgs e)
        {
            lock (LockWCFConnectionTimer)
            {
                if (_WCFinexecution) return;
                _WCFinexecution = true;
            }
            if (this.HostProtocol == Host.TCP) this.HostWCFTCP();
            _WCFinexecution = false;
        }


        //The connectWCF method is called every run time interval (which is set in the Config XML file, default value is 15 second)
        //This method does nothing if the Host Service has already started listening to a port
        public void HostWCFTCP()
        {
            //Check is WCF TCP host exists, if doesn't exist, create the object
            if (this.ServiceHost == null)
            {
                Logger.Log("Creating WCF TCP Service Host Object for '" + typeof(clsType) + "'",this.ProductDetails.LogFilePath);
                try
                {
                    this.ServiceHost = new ServiceHost(typeof(clsType));
                    Logger.Log("Successfully Created WCF Service Host Object for '" + typeof(clsType) + "'", this.ProductDetails.LogFilePath);
                }
                catch (Exception ex)
                {
                    Logger.Log("ERROR: Failed to Create WCF Service for '" + typeof(clsType) + "'\r\n" + ex.ToString(), this.ProductDetails.LogFilePath,true);
                }
            }
            if (this.ServiceHost != null)
            {
                if (this.ServiceHost.State != CommunicationState.Opened)
                {
                    //Open the communication port to start listening to the port                
                    try
                    {
                        Logger.Log("Opening WCF Service Host for '" + typeof(clsType) + "', Listening on Port " + this.ListeningPort, this.ProductDetails.LogFilePath);
                        this.ServiceHost = new ServiceHost(typeof(clsType), new Uri("net.tcp://localhost:" + this.ListeningPort.ToString()));
                        ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                        smb.HttpGetEnabled = false;
                        this.ServiceHost.Description.Behaviors.Add(smb);

                        //Create Binding
                        //BinaryMessageEncodingBindingElement messageEncoding = new BinaryMessageEncodingBindingElement();
                        //TcpTransportBindingElement tcpTransport = new TcpTransportBindingElement();
                        //CustomBinding NTB = new CustomBinding(messageEncoding, tcpTransport);
                        NetTcpBinding NTB = new NetTcpBinding();
                        NTB.MaxReceivedMessageSize = 20000000; //Server.WCFMaxReceivedMessageSize;
                        Binding mexBinding = MetadataExchangeBindings.CreateMexTcpBinding();

                        this.ServiceHost.AddServiceEndpoint(typeof(iContract), NTB, ""); //Add Endpoint to Host
                        this.ServiceHost.AddServiceEndpoint(typeof(iContract), mexBinding, "mex"); //Add Endpoint to Host
                        this.ServiceHost.Open();
                        if (this.ServiceHost.State == CommunicationState.Opened) Logger.Log("Successfully Opened WCF Service Host for '" + typeof(clsType) + "'", this.ProductDetails.LogFilePath);
                    }
                    catch (Exception ex)
                    {
                        Logger.Log("ERROR: Unable to Open WCF Service Host for '" + typeof(clsType) + "'\r\n" + ex.ToString(), this.ProductDetails.LogFilePath,true);
                    }
                }
            }
        }
    }


    public class WCFClient<IContract>
    {

        public WCFClient(ProductDetails ProductDetails, string ServerHost, int ServerPort, int ConnectionRefreshIntervalInMS = 5000, int ConnectionRetryIntervalInMS = 10000)
        {
            ConnectionRefresh.Elapsed += new System.Timers.ElapsedEventHandler(ConnectionRefresh_Elapsed);
            this.ProductDetails = ProductDetails;
            this.ServerHost = ServerHost;
            this.ServerPort = ServerPort;
            this.ConnectionRetryIntervalInMS = ConnectionRetryIntervalInMS;
            this.ConnectionRefreshIntervalInMS = ConnectionRefreshIntervalInMS;
        }
        public WCFClient(string ServerHost, int ServerPort, int ConnectionRefreshIntervalInMS = 5000, int ConnectionRetryIntervalInMS = 10000)
        {
            ConnectionRefresh.Elapsed += new System.Timers.ElapsedEventHandler(ConnectionRefresh_Elapsed);
            this.ProductDetails = ProductDetails;
            this.ServerHost = ServerHost;
            this.ServerPort = ServerPort;
            this.ConnectionRetryIntervalInMS = ConnectionRetryIntervalInMS;
            this.ConnectionRefreshIntervalInMS = ConnectionRefreshIntervalInMS;
            this.EnableActivityLog = false;
        }

        public ProductDetails ProductDetails { get; set; }
        bool _IsEnabled=false; public bool IsEnabled { get { return _IsEnabled; } }
        public string ServerHost { get; set; }
        public int ServerPort { get; set; }
        public ChannelFactory<IContract> WCFFactory { get; set; }
        public bool ConnectionOK = false;
        public IContract Channel = default(IContract);
        public int ConnectionRefreshIntervalInMS { get; set; }
        public int ConnectionRetryIntervalInMS { get; set; }
        System.Timers.Timer ConnectionRefresh = new System.Timers.Timer();
        public bool EnableActivityLog = true;


        public void Start()
        {
            if (!_IsEnabled)
            {
                _IsEnabled = true;
                ConnectionRefresh_Elapsed(null, null); //Established Initial Connection
                ConnectionRefresh.Interval = this.ConnectionRefreshIntervalInMS;
                ConnectionRefresh.Enabled = true;
            }
        }
        public void Stop()
        {
            _IsEnabled = false;
            this.ConnectionOK = false;
            ConnectionRefresh.Stop();
            ConnectionRefresh.Enabled = false;
        }


        //Refreshes the Connection
        readonly object ConnectToServerLock = new Object();
        bool _inConnectToServer = false;
        private void ConnectionRefresh_Elapsed(object sender, EventArgs e)
        {
            lock (ConnectToServerLock) if (_inConnectToServer) return; else _inConnectToServer = true;

            if (_IsEnabled && !this.ConnectionOK)
            {

                if (this.EnableActivityLog) Logger.Log("Attempting to Connect to " + this.ServerHost + ":" + this.ServerPort + "'. Creating Channel Factory...", this.ProductDetails.LogFilePath);
                try
                {
                    NetTcpBinding NTB = new NetTcpBinding();
                    NTB.MaxReceivedMessageSize = 20000000;
                    WCFFactory = new ChannelFactory<IContract>(NTB, new EndpointAddress("net.tcp://" + this.ServerHost + ":" + this.ServerPort));
                    if (this.EnableActivityLog) Logger.Log("Successfully Created WCF Channel Factory. Creating WCF Client...", this.ProductDetails.LogFilePath);
                    Channel = WCFFactory.CreateChannel();
                    if (this.EnableActivityLog) Logger.Log("Connecting to TCP address '" + this.ServerHost + ":" + this.ServerPort + "'", this.ProductDetails.LogFilePath);
                    ((IClientChannel)Channel).Open();
                    if (this.EnableActivityLog) Logger.Log("Successfully Connected to TCP address '" + this.ServerHost + ":" + this.ServerPort + "'", this.ProductDetails.LogFilePath);
                    this.ConnectionOK = true;
                }
                catch (Exception Ex)
                {
                    if (this.EnableActivityLog) Logger.Log("ERROR: Unable to connect to Server. Retrying Connection after " + this.ConnectionRetryIntervalInMS + "ms (ConnectionRetryDelayInMS) \r\n" + Ex.ToString(), "", true);
                    this.ConnectionOK = false;
                    ConnectionRefresh.Stop();
                    System.Threading.Thread.Sleep(this.ConnectionRetryIntervalInMS);
                    ConnectionRefresh.Start();
                }
            }

            _inConnectToServer = false;
        }
    }
}

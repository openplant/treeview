﻿using System;
using System.Windows;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Xml.Serialization;
using System.Diagnostics;
using System.Threading;
using System.Reflection;

namespace OpenPlant
{
    public static class Logger
    {
        public struct LogStruct
        {
            public string LogStr;
            public string DestinationFileName; //If ""(empty) it means that its a Log File
            public bool IsError;
            public string AssemblyStr;
        }
        public static int MaxLogFileSizeInMB = 40; //Default is 40MB
        public static int LogQueueLimit = 100000;
        public static Queue<LogStruct> LoqQueue = new Queue<LogStruct>(Logger.LogQueueLimit);
        public static bool LoggerStarted = false;

        static bool _Disabled = false;
        public static void DisableLogger()
        {
            _Disabled = true;
        }
        static readonly object LogQueueLock = new Object();
        static readonly object WriteLogLock = new Object();
        static readonly object LogLock = new Object();
        static int WritelogCounter = 500; //This is to allow file cheking only when the counter reaches > 250


        public static void ClearLog(string LogFullPath)
        {
            if (File.Exists(LogFullPath))
            {
                File.Delete(LogFullPath);
            }
        }
        public static void EnableLogger()
        {
            LoggerStarted = true;
            _Disabled = false;
            new Thread(Logger.LoggerEngine).Start();
        }



        [DebuggerStepThroughAttribute]
        public static void WriteLog(String FullPath, String Log)
        {
            lock (WriteLogLock)
            {
                WritelogCounter = WritelogCounter + 1;
                try
                {
                    if (WritelogCounter >= 500)
                    {
                        if (File.Exists(FullPath))
                        {
                            FileInfo Fi = new FileInfo(FullPath);
                            if (Fi.Length > Logger.MaxLogFileSizeInMB * 1024 * 1024)
                            {
                                if (File.Exists(FullPath + ".Old")) File.Delete(FullPath + ".Old");
                                File.Move(FullPath, FullPath + ".Old");
                            }
                        }
                        else
                        {
                            string DirPath = Path.GetDirectoryName(FullPath);
                            if (!Directory.Exists(DirPath))
                            {
                                try
                                {
                                    Directory.CreateDirectory(DirPath);
                                    Logger.Log("Logger > Successfully Created Log Directory '" + DirPath + "'", FullPath);
                                }
                                catch (Exception ex)
                                {
                                    Logger.Log("Logger > ERROR: Unable to create Log directory '" + DirPath + "'\r\n" + ex.ToString(), FullPath, true);
                                    Environment.Exit(0);
                                }
                            }
                        }
                        WritelogCounter = 0;
                    }
                    File.AppendAllText(@FullPath, Log);
                }
                catch (Exception ex)
                {
                    Logger.Log("Logger > ERROR: Unable to log." + ex.ToString(), FullPath, true);
                    WritelogCounter = 500;
                }
            }
        }

        //[DebuggerStepThroughAttribute]
        public static void Log(String Entry, String DestinationFileName, bool IsError = false)
        {
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject())) return;
            if (_Disabled) return;
            if (!LoggerStarted)
            {
                Logger.EnableLogger(); // This also set LoggerStarted=true
            }
            lock (LogLock)
            {
                StackFrame SF = (new System.Diagnostics.StackTrace(true)).GetFrame(1);
                string FP = SF.GetFileName();
                string currentFile = System.IO.Path.GetFileName(FP);
                string Header;
                if (currentFile != null)
                {
                    currentFile = currentFile.Substring(0, 5) + "~";
                    int currentLine = new System.Diagnostics.StackTrace(true).GetFrame(1).GetFileLineNumber();
                    Header = DateTime.Now.ToString("dd/MM/yy HH:mm:ss.fff") + "  " + currentFile + currentLine;
                }
                else
                {
                    currentFile = "";
                    Header = DateTime.Now.ToString("dd/MM/yy HH:mm:ss.fff") + "  ";
                }
                string Log = Header.PadRight(35) + Entry.Replace("\r\n", "\r\n".PadRight(54)) + "\r\n";
                if (DestinationFileName == "") throw new CustomException("Log destination not set");

                LogStruct LS;
                LS.LogStr = Log;
                LS.DestinationFileName = DestinationFileName;
                LS.IsError = IsError;
                LS.AssemblyStr = Path.GetFileName(System.Reflection.Assembly.GetCallingAssembly().Location);
                lock (LogQueueLock) LoqQueue.Enqueue(LS);
            }
        }

        public static void LoggerEngine()
        {
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject())) return;
            int LoqQueueCount;
            while (1 == 1)
            {
                lock (LogQueueLock) LoqQueueCount = LoqQueue.Count;
                if (LoqQueueCount == 0)
                {
                    Thread.Sleep(100); //sleep 1ms if no log exists        
                    continue;
                }
                LogStruct LogItem;
                if (_Disabled) //If logger is disabled
                {
                    LoggerStarted = false;
                    return;
                }


                lock (LogQueueLock) LogItem = LoqQueue.Dequeue();
                WriteLog(LogItem.DestinationFileName, LogItem.LogStr);
                if (LogItem.IsError)
                {
                    string LogErrorFullPath = Path.GetDirectoryName(LogItem.DestinationFileName) + "\\" + Path.GetFileNameWithoutExtension(LogItem.DestinationFileName) + "-Error" + Path.GetExtension(LogItem.DestinationFileName);
                    WriteLog(LogErrorFullPath, LogItem.LogStr); //This is to keep error all in one log for easy referencing    
                }
            }
        }


    }


}
